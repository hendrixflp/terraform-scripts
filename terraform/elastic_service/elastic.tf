resource "aws_elasticsearch_domain" "st_es" {
  domain_name           = "stone"
  elasticsearch_version = "7.4"

  cluster_config {
    instance_type = "r4.large.elasticsearch"
  }

  ebs_options {
      ebs_enabled = true
      volume_size = 10
  }

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  tags = {
    Domain = "TestDomain"
  }
}

resource "aws_elasticsearch_domain_policy" "main" {
  domain_name = "${aws_elasticsearch_domain.st_es.domain_name}"

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Condition": {
                "IpAddress": {"aws:SourceIp": "127.0.0.1/32"}
            },
            "Resource": "${aws_elasticsearch_domain.st_es.arn}/*"
        }
    ]
}
CONFIG
}