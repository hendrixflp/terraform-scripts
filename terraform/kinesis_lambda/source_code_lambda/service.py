import base64
from datetime import datetime
import json

from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch, RequestsHttpConnection

class EventsHandler:

   def __init__(self, event, context=None):
      self.event = event
      self.context = context
      self.elastic_client = Elasticsearch(
         hosts="https://search-stone-evhbgalrt74wwqq6o56vtzec4m.us-east-2.es.amazonaws.com",
         connection_class=RequestsHttpConnection
      )


   @staticmethod
   def get_date():
      return datetime.now().strftime("%Y-%m-%d")

   @staticmethod
   def decrypt_data(data):
      return base64.b64decode(data).decode("utf-8")

   def sent_to_es(self, events):
      current_date = self.get_date()

      for event in events['Records']:
         body_event = self.decrypt_data(event['kinesis_lambda']['data'])
         event_json = json.loads(body_event)

         event_json.update({
            '_index': f'data-{current_date}',
            '_type': "_doc",
         })

         print(str(event_json))
         yield event_json


   def send(self, events):

      print('Processando bulk')
      start = datetime.now()

      try:

         bulk(
            client=self.elastic_client,
            actions=self.sent_to_es(events),
            request_timeout=10,
            max_retries=3,
            chunk_size=3000
         )


         finish = datetime.now()
         duration = finish - start

         print('Bulk sucess - Duration ' + str(duration.seconds) + ' secs ' + ' : ' + str(duration.microseconds) + ' microsecs')

      except Exception as e:

         print(' Error on indexing ' + str(e))



def handler(event, context):
   events_handler = EventsHandler(event=event, context=context)
   events_handler.send(event)