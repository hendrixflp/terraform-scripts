import boto3
import json
from datetime import datetime
import time
import pytz
import random
import uuid

BATCH_SIZE = 500
PLATFORM_STREAM = 'kinesis-stone'

PRODUCTS = ["Maquina", "AntiFraude", "Bank"]
USER_TYPE = ["PF", "PJ"]
TIPO_USO = ["Consumo", "Relatório"]
MEDIA_TYPE = ["Database", "Cloud", "Code", "Job", "Backend", "Frontend"]
ORIGIN_TYPE = ["Entrada", "Saida"]
MODE = ["Single", "Batch"]
CLIENTS = ["Cliente", "Fornecedor"]
FINALITIES = ["Credito", "Marketing", "RH"]

kinesis_client = boto3.client('kinesis', region_name='us-east-2')

def put_to_stream(partition_key, property_value, json_events, timestamp):
    payload = {
                'prop': str(property_value),
                'timestamp': str(timestamp),
                'event' : json_events
              }


    print('Putting Object to Kinesis ' + PLATFORM_STREAM)

    print("Payload")
    print(str(json.dumps(payload)))

    response = kinesis_client.put_record(
            StreamName=PLATFORM_STREAM,
            Data=json.dumps(payload),
            PartitionKey=partition_key)

    print(str(response))

if __name__ == '__main__':

    count = 0
    #while True:
    while count < BATCH_SIZE:
        now = datetime.now(tz=pytz.timezone('America/Sao_Paulo')).isoformat(),
        property_value = random.randint(40, 120)
        timestamp = now
        partition_key = str(uuid.uuid4())

        json_event = {
            "ID": random.randint(40, 120),
            "DataHora": now,
            "Validade": "2021-08-25T18:25:43Z",
            "TipoDeConsulta": "Externa",
            "Finalidades": [
                random.choice(FINALITIES)
            ],
            "Produto": random.choice(PRODUCTS),
            "TipoDeUso": [
                random.choice(TIPO_USO)
            ],
            "Arquivo": {
                "Diretorio": "c:/teste/",
                "Midia": [
                    random.choice(MEDIA_TYPE)
                ],
                "Nome": "DadosPessoaFisica.csv"
            },
            "Origem": {
                "CodigoMeioDeAcesso": 24,
                "Tipo": [
                    random.choice(ORIGIN_TYPE)
                ],
                "Modo": [
                    random.choice(MODE)
                ]
            },
            "Pessoa": {
                "Tipo": [
                   random.choice(USER_TYPE)
                ],
                "Registro": "00000000000"
            },
            "Empresa": {
                "Tipo": [
                    "Cliente"
                ],
                "RazaoSocial": "Serasa Ltda.",
                "CNPJ": "44083494000190"
            },
            "Template": {
                "Codigo": "B49C",
                "Versao": "1.0"
            },
            "Conteudo": "Entrada de log gerada pela aplicação"
        }

        put_to_stream(partition_key, property_value, json_event, timestamp)
        count = count + 1

        time.sleep(1)


    print("Done")