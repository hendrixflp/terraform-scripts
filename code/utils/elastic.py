import os
import base64
import json
from datetime import datetime

from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch, RequestsHttpConnection

ELASTIC_HOST = "https://platform-dev-kd5tbphgxjosy4p5dk5vqiyynm.us-east-1.es.amazonaws.com"

class EventsHandler:

   def __init__(self, event, context=None):
      self.event = event
      self.context = context
      self.elastic_client = Elasticsearch(
         hosts=ELASTIC_HOST,
         connection_class=RequestsHttpConnection
      )

   @staticmethod
   def get_date():
      return datetime.now().strftime("%Y-%m-%d")

   @staticmethod
   def decrypt_data(data):
      return base64.b64decode(data).decode("utf-8")

   def sent_to_es(self, events):
      current_date = self.get_date()

      print('Payload')
      events.update({
         '_id': "10000",
         '_version': 1,
         '_seq_no': 0,
         '_primary_term': 1,
         '_index': f'data-{current_date}',
         '_type': "_doc"
      })

      print(str(events))
      yield events

   def send(self, events):
      bulk(
         client=self.elastic_client,
         actions=self.sent_to_es(events),
         request_timeout=2,
         max_retries=5,
         chunk_size=5000
      )


def handler(event, context):
   events_handler = EventsHandler(event=event, context=context)
   events_handler.send(event)


if __name__ == '__main__':

   current_path = os.path.dirname(os.path.abspath(__file__))

   with open(f'{current_path}/event.json') as file:
      raw_event = file.read()
      json_events = json.loads(raw_event)

      handler(json_events, None)
      print('Sent!')