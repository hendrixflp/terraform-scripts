[Plataforma de Logs LGPD]()
---


####DESCRIÇÃO DO FLUXO

Nossas aplicações/produtos utilizam sdk’s da AWS que permitem o uso do Kinesis viabilizando envio de LOGS de forma centralizada e 
padronizada, sempre respeitando a exigência de envio dos LOGS no formato exigido pela LGPD, abaixo desenho da proposta de arquitetura 
que será utilizada, e padrão de LOG LGPD.

Abaixo segue exemplo de payload seguindo os padrões e informações esperados pela LGPD

```
      "Tipo":[
         "Entrada"
      ],
      "Modo":[
         "Batch"
      ]
   },
   "Pessoa":{
      "Tipo":[
         "PessoaFisica",
         "PessoaJuridica"
      ],
      "Registro":"00000000000"
   },
   "Empresa":{
      "Tipo":[
         "Cliente"
      ],
      "RazaoSocial":"Serasa Ltda.",
      "CNPJ":"44083494000190"
   },
   "Template":{
      "Codigo":"B49C",
      "Versao":"1.0"
   },
   "Conteudo":"Entrada de log gerada pela aplicação"
}
```

### Arquitetura

![Arq](arquitetura.jpg)


Conforme imagem acima esse envio irá realizar o processamento de tráfego através da solução do Kinesis, que é amplamente utilizada para stream de dados em larga escala, 
por sua vez o Kinesis terá um conector que fica responsável por processar todos os LOGS enviando para um S3 que registrará todos os logs para eventuais 
consultas a nível histórico.

O Kinesis terá também um integração com uma componente que ficará responsável por controle e gestão de todos os logs enviados, tendo a responsabilidade de controlar o fluxo, 
com foco em consistência, resiliência e escalabilidade do tráfego, conectando e alimentado o ElasticSearch, aonde poderá realizar consultas, acompanhamentos e construção de relatórios
como descrito abaixo: 

### Relatórios

![Arq](reports.png)

